class BangunDatar{
  constructor(){
    if (this.constructor === BangunDatar) {
      throw new Error("An Error");
    }
  }
  
  Luas(){

  }

  Keliling(){

  }
}

class Persegi extends BangunDatar{
  constructor(s){
    super();
    this.s = s;
  }

  Luas(){
    return this.s*this.s;
  }

  Keliling(){
    return 4*this.s;
  }
}

class PersegiPanjang extends BangunDatar{
  constructor(p, l){
    super();
    this.p = p;
    this.l = l;
  }

  Luas(){
    return this.p*this.l;
  }

  Keliling(){
    return 2*(this.p+this.l);;
  }
}

class Segitiga extends BangunDatar{
  constructor(a, t, c){
    super();
    this.a = a;
    this.t = t;
    this.c = c;
  }

  Luas(a, t){
    return this.a*this.t/2;
  }

  Keliling(){
    return this.a+this.t+this.c;
  }
}

let persegi1 = new Persegi(5);
console.log("luas persegi: ", persegi1.Luas());
console.log("keliling persegi: ", persegi1.Keliling());

let persegipanjang1 = new PersegiPanjang(6, 8);
console.log("luas persegi panjang: ", persegipanjang1.Luas());
console.log("keliling persegi panjang: ", persegipanjang1.Keliling());

let segitiga1 = new Segitiga(5, 6, 7);
console.log("luas segitiga: ", segitiga1.Luas());
console.log("keliling segitiga: ", segitiga1.Keliling());