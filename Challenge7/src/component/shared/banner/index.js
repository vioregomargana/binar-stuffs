import React, {useEffect} from 'react';
import carBg from '../../assets/images/carbg.png';
const Banner = () => {

    return (
        <div>
            {/* <!-- Navigation Bar --> */}
            <nav className="full bgblue nav-bg">
                <div className="inContent row">
                    <nav className="navbar navbar-expand-lg navbar-light">
                        <div className="container-fluid ">
                            <a href="#/" className="navbar-brand nav-blue-btn">
                                <div className="logo"></div>
                            </a>
                            <button className="navbar-toggler navbar-toggler-icon" type="button" data-bs-toggle="offcanvas"
                                data-bs-target="#navbarNavAltMarkup" style={{border: 0}}></button>
                            <div className="offcanvas offcanvas-end navbar-nav" id="navbarNavAltMarkup">
                                <div className="offcanvas-header">
                                    <h5 id="offcanvasRightLabel"><b>BCR</b></h5>
                                    <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas"
                                        aria-label="Close"></button>
                                </div>
                                <div className="col navbar-expand-sm navbar nav-pad" style={{columnGap: "32px", rowGap: "16px", left: "50%"}}>
                                    <a className="nav-item navbar-brand" href="#OurServices">Our Services</a>
                                    <a className="nav-item navbar-brand" href="#WhyUs">Why Us</a>
                                    <a className="nav-item navbar-brand" href="#Testimonial">Testimonial</a>
                                    <a className="nav-item navbar-brand" href="#FAQ">FAQ</a>
                                    <button className="nav-link btnRegister" id="btn">Register</button>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </nav>

            {/* <!-- Main --> */}

            <section>
                <div className="full bgblue nav-bg">
                    <div className="main row">
                        <div className="col-12 col-lg-6" style={{marginTop: "10%", marginBottom: "auto"}}>
                            <div className="mainText ">
                                <h1 style={{maxWidth: "568px"}}>Sewa & Rental Mobil Terbaik di kawasan Bali</h1>
                                <p style={{maxWidth: "460px"}}>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik
                                    dengan harga terjangkau. Selalu
                                    siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                                <a href="/sewa">
                                    <button className="btn btn-success text-white" id="btn">Mulai Sewa Mobil</button>
                                </a>
                            </div>
                        </div>
                        {/* <img className="carImage" src="/assets/mobil.png" alt="" /> */}
                        <div class="col-sm-6">
                            <img className="image1 blue-bg" src={carBg} />
                        </div>
                    </div>
                    {/* <div className="blueBackground vw48 "></div>
                    <div className="blueBackground vw90"></div> */}
                </div>
            </section>

        </div>
    );
}

export default Banner;