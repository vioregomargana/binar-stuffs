import arrow from '../../../assets/images/arrow.png'
const Faq = () => {
    return (
        <>
            <div className="inContent row" style={{marginTop: "100px"}}>
                <div className="col-12 col-md-5 text-center text-md-start top-margin">
                    <h2>Frequently Asked Question</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                </div>
                <div className="col-12 col-md-7 card">
                    <button className="faqCard d-flex justify-content-between align-middle card-header" style={{marginTop: "10px"}}>
                        <p style={{margin: "0px"}}>Apa saja syarat yang dibutuhkan?</p>
                        <img className="icon12" src={arrow} alt="more" />
                    </button>
                    <div className="panel">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis dolore officiis itaque
                        eius quos voluptatem culpa! Excepturi ipsum, labore quo officiis, suscipit, ut vitae in eius reprehenderit
                        earum ab dignissimos!</div>
                    <button className="faqCard d-flex justify-content-between align-middle card-header" style={{marginTop: "10px"}}>
                        <p style={{margin: "0px"}}>Berapa hari minimal sewa mobil lepas kunci?</p>
                        <img className="icon12" src={arrow} alt="more" />
                    </button>
                    <div className="panel">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis dolore officiis itaque
                        eius quos voluptatem culpa! Excepturi ipsum, labore quo officiis, suscipit, ut vitae in eius reprehenderit
                        earum ab dignissimos!</div>
                    <button className="faqCard d-flex justify-content-between align-middle card-header" style={{marginTop: "10px"}}>
                        <p style={{margin: "0px"}}>Berapa hari sebelumnya sebaiknya booking sewa mobil?</p>
                        <img className="icon12" src={arrow} alt="more" />
                    </button>
                    <div className="panel">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis dolore officiis itaque
                        eius quos voluptatem culpa! Excepturi ipsum, labore quo officiis, suscipit, ut vitae in eius reprehenderit
                        earum ab dignissimos!</div>
                    <button className="faqCard d-flex justify-content-between align-middle card-header" style={{marginTop: "10px"}}>
                        <p style={{margin: "0px"}}>Apakah ada biaya antar-jemput?</p>
                        <img className="icon12" src={arrow} alt="more" />
                    </button>
                    <div className="panel">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis dolore officiis itaque
                        eius quos voluptatem culpa! Excepturi ipsum, labore quo officiis, suscipit, ut vitae in eius reprehenderit
                        earum ab dignissimos!</div>
                    <button className="faqCard d-flex justify-content-between align-middle card-header" style={{marginTop: "10px"}}>
                        <p style={{margin: "0px"}}>Bagaimana jika terjadi kecelakaan?</p>
                        <img className="icon12" src={arrow} alt="more" />
                    </button>
                    <div className="panel">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis dolore officiis itaque
                        eius quos voluptatem culpa! Excepturi ipsum, labore quo officiis, suscipit, ut vitae in eius reprehenderit
                        earum ab dignissimos!</div>
                </div>
            </div>

        </>
    );
}

export default Faq;