const CtaBanner = () => {
    return (
        <>
        <div class="container top-margin">
            <div class="sewa-cont">
                <div class="sewa-cont-2">
                    <p class="sewa-title">Sewa Mobil di Tangerang Sekarang</p>
                    <small class="sewa-text">Lorem ismallsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua.</small>
                    <a href="/sewa" role="button" class="btn btn-success text-white sewa-btn">Mulai Sewa Mobil</a>
                </div>
            </div>
        </div>

        </>
    );
}

export default CtaBanner;