const express = require('express');
const app = express();
const path = require('path');
const PORT = 8000;
const PATH_DIR = __dirname + '/public/';

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(path.join(PATH_DIR + 'index.html'));
});

app.get('/challenge4', (req, res) => {
    res.sendFile(path.join(PATH_DIR + 'challenge4.html'));
});

app.listen(PORT, () => console.log(`Server running at localhost:${PORT}`));