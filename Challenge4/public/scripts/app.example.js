class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.submitButton = document.getElementById("submit-btn");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
    this.submitButton.onclick = this.filterCar;
  }

  run = () => {
    Car.list.forEach((car) => {
      // const node = document.createElement("div");
      // node.innerHTML = car.render();
      // this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;

      Car.list.forEach((car) => {
        const node = document.createElement("div");
        node.innerHTML = car.render();
        this.carContainerElement.appendChild(node);
      });
    }
  };

  filterCar = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }

    let driverValue = document.getElementById("tipeDriverInput").value;
    let jumlahPenumpang = document.getElementById("jumlahPenumpangInput").value;
    let dateValue = document.getElementById("date").value;
    let timeValue = document.getElementById("time").value;
    let dateRent = (dateValue + "T" + timeValue);
    let formDate = Date.parse(dateRent);

    Car.list.forEach((car) => {

      let dateRenting = Date.parse(car.availableAt)
      let dateTime = car.availableAt
      let time = dateTime.toLocaleTimeString();

      // to split the date and time
      let tempdate = JSON.stringify(car.availableAt);
      let tempdate2 = tempdate.split("T");
      let tempdate3 = tempdate2[0].replace('"', '');
      

      if (jumlahPenumpang <= car.capacity && driverValue == car.available && time >= timeValue && tempdate3 >= dateValue) {
        if (dateRenting >= formDate) {
          const node = document.createElement("div");
          node.innerHTML = car.render();
          this.carContainerElement.appendChild(node);
        }
        if (driverValue == car.available) {
          const node = document.createElement("div");
          node.innerHTML = car.render();
          this.carContainerElement.appendChild(node);
        }
      } else if (driverValue == "null" && jumlahPenumpang <= car.capacity) {
        if (dateRenting >= formDate || time >= timeValue && tempdate3 >= dateValue) {
          const node = document.createElement("div");
          node.innerHTML = car.render();
          this.carContainerElement.appendChild(node);
        }
      }
    })
  };
}