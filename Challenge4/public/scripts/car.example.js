class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
  <section class="card">
    <section class="card-body">
      <section>
        <img src="${this.image}" width="100%">
      </section>
      <section id="car-spec">
        <section>
          <p>${this.manufacture} ${this.model}</p>
          <h6>Rp ${this.rentPerDay} / hari</h6>
          <p>${this.description}</p>
        </section>
        <section class="car-spec-icon">
          <p>
            <i class="bi bi-people"></i>
            ${this.capacity} orang
          </p>
          <p>
            <i class="bi bi-gear"></i>
            ${this.transmission}
          </p>
          <p>
            <i class="bi bi-calendar"></i>
            Tahun ${this.year}
          </p>
          <button class="btn btn-success car-spec-btn">Pilih Mobil</button>
        </section>
      </section>
    </section>
  </section>`;
  }
}