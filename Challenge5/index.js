const express = require('express');
const app = express();
const formidable = require('express-formidable');
const path = require('path');
const PORT = 8000;
const PATH_DIR = __dirname + '/public/';
const carsController = require('./controller/cars.controller.js');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use(formidable({
    uploadDir: 'uploads'
}));
app.use(express.static('public'));
app.use(express.static('uploads'));


// Controller cars
// Render view
app.get('/cars', carsController.getAllCars);
app.get('/cars/add', carsController.renderCreateCarForm);
app.get('/cars/update/:id', carsController.renderUpdateCarForm);
app.get("/filter/:ukuran", carsController.renderFilter);

// Endpoint logic
app.post('/cars', carsController.createNewCar);
app.post('/cars/:id', carsController.updateCar);
app.get('/cars/:id', carsController.deleteCar);

app.listen(PORT, () => console.log(`Server running at localhost:${PORT}`));